const globalParam = {};
// 全局变量
const dict = {};
class GlobalEvent {
  // 全局事件
  constructor() {
    this.id = 1;
    this.events = {};
    this.idform = {};
  }

  trigger(name, param) {
    const eventList = this.events[name];
    if (eventList) {
      for (const key in eventList) {
        eventList[key].call(null, param);
      }
    }
  }

  haslistener(name) {
    if (this.events[name] && Object.keys(this.events[name]).length) {
      return true;
    }
    return false;
  }

  addlistener(name, func) {
    const id = this.id;
    if (!this.events[name]) this.events[name] = {};
    this.events[name][id] = func;
    this.idform[id] = name;
    this.id++;
    return id;
  }

  removelistener(id) {
    if (this.idform[id]) {
      delete this.events[this.idform[id]][id];
      delete this.idform[id];
    }
  }
}

const globalEvent = new GlobalEvent();

const G = {
  getDict(name) {
    if (name == undefined) return name;
    return dict[name];
  },
  saveDict(name, value) {
    if (name == undefined) return false;
    dict[name] = value;
  },
  get(name) {
    if (name == undefined) return name;
    return globalParam[name];
  },
  set(name, value) {
    if (name == undefined) return false;
    globalParam[name] = value;
  },
  trigger(name, param) {
    globalParam.trigger(name, param);
  },
  addlistener(name, func) {
    return globalEvent.addlistener(name, func);
  },
  haslistener(name) {
    return globalEvent.haslistener(name);
  },
  removelistener(id) {
    globalEvent.removelistener(id);
  }
};

module.exports = G;
